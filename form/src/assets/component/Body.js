import { Box, Container, Typography } from "@mui/material";
import React, { useState } from "react";
import "../css/Body.css";

export const Body = ({ save, name, address, hobby }) => {
  const [ArrayData, setArrayData] = useState([]);

  console.log("BODYYYY is Save " + save);
  let dataHandle = ArrayData ? ArrayData : [];
  if (save) {
    dataHandle.push(
      {nama: name, alamat: address, hobi: hobby}
    );
    setArrayData(dataHandle);
  }

  console.log("BODYYYY data " + dataHandle);

  const handleData = () => {
    let dataHandle = ArrayData ? ArrayData : [];
    if (dataHandle.length === 0) {
      return (
        <Container maxWidth="md" sx={{ position: "relative", zIndex: 1000 }}>
          <div className="imgCenter">
            <img src="https://external-preview.redd.it/9LjM8rKqaZ7mVwe5JbY9i7lRVb-qiMR_wCKo1UMBJ_I.jpg?auto=webp&s=4d70ec407d02dd0bcda5e8af46458a214e6d0aaf"></img>
          </div>
          <Typography
            variant="h2"
            component="h1"
            color="common.black"
            fontWeight="600"
            textAlign="center"
          >
            USER
          </Typography>
        </Container>
      );
    } else {
      return <div>Data</div>;
    }
  };

  return <Box sx={{ flexGrow: 1 }}>{handleData()}</Box>;
};
