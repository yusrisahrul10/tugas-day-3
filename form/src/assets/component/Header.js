import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { green } from "@mui/material/colors";
import { styled } from "@mui/material";

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(green[500]),
  backgroundColor: green[500],
  "&:hover": {
    backgroundColor: green[700],
  },
}));

export const Header = ({ OpenModal, SaveUser }) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            My App
          </Typography>
          <ColorButton
            variant="contained"
            sx={{ textTransform: "none", ml: 6 }}
            onClick={() => {
              OpenModal();
              SaveUser();
            }}
          >
            Add User
          </ColorButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
