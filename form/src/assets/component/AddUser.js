import React, { useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import TextField from '@mui/material/TextField';
import { Grid, styled } from "@mui/material";
import { green } from "@mui/material/colors";

const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(green[700]),
    backgroundColor: green[700],
    "&:hover": {
      backgroundColor: green[990],
    },
    alignItems: "center"
  }));

export const AddUser = ({ open, CloseModal, AddNama, AddAddress, AddHobby, name, address, hobby, SaveUser}) => {
    // const [nama, setNama] = useState("");
    // const [alamat, setAlamat] = useState("");
    // const [hobi, setHobi] = useState("");

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };
  return (
    
    <Modal open={open} onClose={CloseModal}>
        
      <Box sx={style}>
        <Typography variant="h4" component={"h1"} textAlign={"center"}>Add User</Typography>
        <form>
        <Typography variant="h6" component={"h1"}>Name</Typography>
        <TextField
            id ="outlined-basic"
            label="Name"
            variant="outlined"
            fullWidth
            value={name}
            onChange={(e) => {AddNama(e.target.value);}}
          />
          <Typography variant="h6" component={"h1"}>Address</Typography>
          <TextField
            id ="outlined-basic"
            label="Address"
            variant="outlined"
            fullWidth
            value={address}
            onChange={(e) => {AddAddress(e.target.value);}}
          />
          <Typography variant="h6" component={"h1"}>Hobby</Typography>
          <TextField
            id ="outlined-basic"
            label="Hobby"
            variant="outlined"
            fullWidth
            value={hobby}
            onChange={(e) => {AddHobby(e.target.value);}}
          />
          <Grid container justifyContent="center">
            
          <ColorButton
            variant="contained"
            sx={{ textTransform: "none", mt: 3 }}
            onClick={() => {
                CloseModal();
                SaveUser();
            }}
          >
            Save
          </ColorButton></Grid>
        </form>
        
      </Box>
    </Modal>
  );
};
