import {
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Grid,
  Modal,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import React, { useState } from "react";
import { Header } from "../assets/component/Header";
import { Body } from "../assets/component/Body";
import { AddUser } from "../assets/component/AddUser";
import { green } from "@mui/material/colors";

export const Form = () => {
  const [open, setOpen] = useState(false);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [hobi, setHobi] = useState("");
  const [save, setSave] = useState(false);
  // const [ArrayData, setArrayData] = useState([{}]);
  const [ArrayData, setArrayData] = useState([]);
  const [Submit, setSubmit] = useState(false);

  console.log("Nama " + nama);
  console.log("Alamat " + alamat);
  console.log("hobi " + hobi);

  console.log("save " + save);

  // console.log("Array Data ")

  const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(green[700]),
    backgroundColor: green[700],
    "&:hover": {
      backgroundColor: green[990],
    },
    alignItems: "center",
  }));

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const AddArray = () => {
    let dataHandle = ArrayData ? ArrayData : [];
    dataHandle.push({ name: nama, address: alamat, hobby: hobi });
    setArrayData(dataHandle);
    setSubmit(!Submit);
  };

  const handleBody = () => {
    let dataHandle = ArrayData ? ArrayData : [];
    if (dataHandle.length === 0) {
      return (
        <Container maxWidth="md" sx={{ position: "relative", zIndex: 1000 }}>
          <div className="imgCenter">
            <img src="https://external-preview.redd.it/9LjM8rKqaZ7mVwe5JbY9i7lRVb-qiMR_wCKo1UMBJ_I.jpg?auto=webp&s=4d70ec407d02dd0bcda5e8af46458a214e6d0aaf"></img>
          </div>
          <Typography
            variant="h2"
            component="h1"
            color="common.black"
            fontWeight="600"
            textAlign="center"
          >
            USER
          </Typography>
        </Container>
      );
    } else {
      return dataHandle.map((value, index) => {
        return (
          <Card
            sx={{
              borderRadius: 7,
              marginTop: 3,
            }}
          >
            <CardContent>
              <Grid container sx={{ alignItems: "center" }}>
                <Grid item xs={10}>
                  <Typography
                    component="span"
                    sx={{
                      fontSize: 20,
                      fontWeight: 600,
                    }}
                  >
                    {value.name}
                  </Typography>
                  <Typography
                    sx={{
                      fontSize: 20,
                      color: "#909090",
                    }}
                  >
                    {value.address}
                  </Typography>
                </Grid>
                <Grid item xs={2} sx={{ textAlign: "center" }}>
                  <Typography
                    sx={{
                      fontSize: 20,
                      fontWeight: 540,
                    }}
                  >
                    {value.hobby}
                  </Typography>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
          // <Card sx={{ minWidth: 275, marginTop: 2}}>
          //   <CardContent gutterBottom>
          //     <Typography variant="h5" component="div">
          //       {value.name}
          //     </Typography>

          //     <Typography variant="h5" component="div">
          //       {value.address}
          //     </Typography>

          //     <Typography variant="h5" component="div">
          //       {value.hobby}
          //     </Typography>
          //   </CardContent>
          // </Card>
        );
      });
    }
  };

  return (
    /* 1-12 */
    /* xs HP sm IPAD md & xl laptop*/
    <>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2} className="Layout">
          <Grid className="box1" item xs={12} sm={3} md={12} xl={12}>
            <Header
              OpenModal={() => {
                setOpen(true);
                setNama("");
                setAlamat("");
                setHobi("");
              }}
              SaveUser={() => setSave(false)}
            />
          </Grid>
          <Grid item xs={12} sm={3} md={12} xl={12} alignItems="center">
            {handleBody()}
          </Grid>
        </Grid>
      </Box>
      <Modal open={open} onClose={() => setOpen(false)}>
        <Box sx={style}>
          <Typography variant="h4" component={"h1"} textAlign={"center"}>
            Add User
          </Typography>
          <form>
            <Typography variant="h6" component={"h1"}>
              Name
            </Typography>
            <TextField
              id="outlined-basic"
              label="Name"
              variant="outlined"
              fullWidth
              value={nama}
              onChange={(e) => {
                setNama(e.target.value);
              }}
            />
            <Typography variant="h6" component={"h1"}>
              Address
            </Typography>
            <TextField
              id="outlined-basic"
              label="Address"
              variant="outlined"
              fullWidth
              value={alamat}
              onChange={(e) => {
                setAlamat(e.target.value);
              }}
            />
            <Typography variant="h6" component={"h1"}>
              Hobby
            </Typography>
            <TextField
              id="outlined-basic"
              label="Hobby"
              variant="outlined"
              fullWidth
              value={hobi}
              onChange={(e) => {
                setHobi(e.target.value);
              }}
            />
            <Grid container justifyContent="center">
              <ColorButton
                variant="contained"
                sx={{ textTransform: "none", mt: 3 }}
                onClick={() => {
                  setOpen(false);
                  AddArray();
                }}
              >
                Save
              </ColorButton>
            </Grid>
          </form>
        </Box>
      </Modal>
      {/* <AddUser
        open={open}
        CloseModal={() => setOpen(false)}
        AddNama={(e) => setNama(e)}
        AddAddress={(e) => setAlamat(e)}
        AddHobby={(e) => setHobi(e)}
        name={nama}
        address={alamat}
        hobby={hobi}
        SaveUser={() => setSave(true)}
      ></AddUser> */}
    </>
  );
};
